package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.*;

public class GUI extends JFrame{
	// requested setting an ID by JFrame
	private static final long serialVersionUID = 1L;
	private int width = 600, height = 600;
	private int horizontalOffset = 4;
	
	private BufferedImage bi;
	private Graphics2D g;
	
	private JPanel p;
	private JButton startButton;
	
	private JTextField timeText;
	private JTextField delayMSText;
	private JTextField maxCustomersText;
	private JTextField nrQueuesText;
	private JTextField maxQueuesText;
	private JTextField customerSpawnRateText;
	private JTextField maxServiceTimeText;
	
	private JLabel timeLabel;
	private JLabel delayMSLabel;
	private JLabel maxCustomersLabel;
	private JLabel nrQueuesLabel;
	private JLabel maxQueuesLabel;
	private JLabel customerSpawnRateLabel;
	private JLabel maxServiceTimeLabel;
	
	
	private Color getColorFromIDString(String ID, int queue, int nrQueues){
		int length = ID.length();
		int sum = 0;
		for(int i = 0; i < length; i++){
			if(Character.isDigit(ID.charAt(i)))
				sum += ID.charAt(i);
		}
		
		return new Color(sum%10 * 20, 120, 240 * queue / nrQueues);
	}
	
	public GUI(){
		// getting the screen resolution in order to place the window in the middle
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		
		height = screenSize.height * 93 / 100;
		width = screenSize.width * 95 / 100;	
		
		setSize(width, height);
		setLocation(30, 20);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(null);
		setResizable(false);
		setTitle("Queues");
		
		p = new JPanel();
		p.setSize(width * 163 / 180, height);
		p.setLocation(this.getLocation().x - 30, this.getLocation().y - 23);
		p.setBackground(Color.BLACK);
		p.setLayout(null);
		
		
		//init the start button
		startButton = new JButton("Start");
		startButton.setLocation(width * 164 / 180, height * 14 / 16);
		startButton.setSize(width * 1 / 13, height / 17);
		this.add(startButton);
		
		//init time textbox --------------------------------------------------------------------------------------------
		timeText = new JTextField();
		timeText.setLocation(width * 164 / 180, height * 1 / 10);
		timeText.setSize(width * 1 / 13 , height / 35);
		timeText.setText("400");
		add(timeText);
		
		//init delayMS textbox
		delayMSText = new JTextField();
		delayMSText.setLocation(width * 164 / 180, height * 2 / 10);
		delayMSText.setSize(width * 1 / 13 , height / 35);
		delayMSText.setText("150");
		add(delayMSText);
		
		
		//init maxCustomersText textbox
		maxCustomersText = new JTextField();
		maxCustomersText.setLocation(width * 164 / 180, height * 3 / 10);
		maxCustomersText.setSize(width * 1 / 13 , height / 35);
		maxCustomersText.setText("10");
		add(maxCustomersText);
		
		//init nrQueuesText textbox
		nrQueuesText = new JTextField();
		nrQueuesText.setLocation(width * 164 / 180, height * 4 / 10);
		nrQueuesText.setSize(width * 1 / 13 , height / 35);
		nrQueuesText.setText("5");
		add(nrQueuesText);
		
		//init maxQueuesText textbox
		maxQueuesText = new JTextField();
		maxQueuesText.setLocation(width * 164 / 180, height * 5 / 10);
		maxQueuesText.setSize(width * 1 / 13 , height / 35);
		maxQueuesText.setText("15");
		add(maxQueuesText);
		
		//init customerSpawnRateText textbox
		customerSpawnRateText = new JTextField();
		customerSpawnRateText.setLocation(width * 164 / 180, height * 6 / 10);
		customerSpawnRateText.setSize(width * 1 / 13 , height / 35);
		customerSpawnRateText.setText("50");
		add(customerSpawnRateText);
		
		//init maxServiceTimeText textbox
		maxServiceTimeText = new JTextField();
		maxServiceTimeText.setLocation(width * 164 / 180, height * 7 / 10);
		maxServiceTimeText.setSize(width * 1 / 13 , height / 35);
		maxServiceTimeText.setText("80");
		add(maxServiceTimeText);
		
		//init the time label ---------------------------------------------------------------------------
		timeLabel = new JLabel("Simulation Time");
		timeLabel.setLocation(width * 164 / 180, height * 07 / 100);
		timeLabel.setSize(width * 1 / 13 , height / 30);
		timeLabel.setFont(new Font(timeLabel.getFont().getName(), Font.PLAIN, 10));
		add(timeLabel);
		
		//init the delayMSLabel label
		delayMSLabel = new JLabel("<html>Delay(MS) between<br> clock cycles");
		delayMSLabel.setLocation(width * 164 / 180, height * 16 / 100);
		delayMSLabel.setSize(width * 1 / 13 , height / 30);
		delayMSLabel.setFont(new Font(delayMSLabel.getFont().getName(), Font.PLAIN, 10));
		add(delayMSLabel);
		
		//init the maxCustomersLabel label
		maxCustomersLabel = new JLabel("<html>Maximum number<br> of customers");
		maxCustomersLabel.setLocation(width * 164 / 180, height * 26 / 100);
		maxCustomersLabel.setSize(width * 1 / 13 , height / 30);
		maxCustomersLabel.setFont(new Font(maxCustomersLabel.getFont().getName(), Font.PLAIN, 10));
		add(maxCustomersLabel);
		
		//init the nrQueuesLabel label
		nrQueuesLabel = new JLabel("Initial number of queues");
		nrQueuesLabel.setLocation(width * 164 / 180, height * 37 / 100);
		nrQueuesLabel.setSize(width * 1 / 13 , height / 30);
		nrQueuesLabel.setFont(new Font(nrQueuesLabel.getFont().getName(), Font.PLAIN, 10));
		add(nrQueuesLabel);
		
		//init the maxQueuesLabel label
		maxQueuesLabel = new JLabel("Max number of queues");
		maxQueuesLabel.setLocation(width * 164 / 180, height * 47 / 100);
		maxQueuesLabel.setSize(width * 1 / 13 , height / 30);
		maxQueuesLabel.setFont(new Font(maxQueuesLabel.getFont().getName(), Font.PLAIN, 10));
		add(maxQueuesLabel);
		
		//init the customerSpawnRateLabel label
		customerSpawnRateLabel = new JLabel("<html>Chance(%) for customer<br>to spawn on clock cycle");
		customerSpawnRateLabel.setLocation(width * 164 / 180, height * 56 / 100);
		customerSpawnRateLabel.setSize(width * 1 / 13 , height / 30);
		customerSpawnRateLabel.setFont(new Font(customerSpawnRateLabel.getFont().getName(), Font.PLAIN, 10));
		add(customerSpawnRateLabel);
		
		//init the maxServiceTimeLabel label
		maxServiceTimeLabel = new JLabel("<html>Max Service Time<br> for customers");
		maxServiceTimeLabel.setLocation(width * 164 / 180, height * 66 / 100);
		maxServiceTimeLabel.setSize(width * 1 / 13 , height / 30);
		maxServiceTimeLabel.setFont(new Font(maxServiceTimeLabel.getFont().getName(), Font.PLAIN, 10));
		add(maxServiceTimeLabel);
		
		
		add(p);
	}
	
	public void updateInfo(ArrayList<String[][]> queues, ArrayList<String[]> nonClientData, int clock){
		String text;
		int x;
		int y;
		
		int horizScale = 16; // max nr of queues
		int vertScale = 17; // max nr of clients per queue minus one
		bi = new BufferedImage(p.getWidth(), height, BufferedImage.TYPE_INT_RGB);
		g = (Graphics2D) bi.getGraphics();
		
		// clock
		g.setColor(Color.WHITE);
		g.drawString("Time: " + clock + " / " + timeText.getText(), 7, 18);
		
		for(int i = 0; i < queues.size(); i++){
			for(int j = 0; j < queues.get(i).length; j++){
				
				// rectangle containing the "client"
				g.setColor(getColorFromIDString(queues.get(i)[j][0], i, queues.size()));
				g.fillRect(i * p.getWidth() / horizScale + horizontalOffset - 2, 
						height / vertScale * (vertScale - (j + 2)) - 17, 
						p.getWidth() / 16 - 5, height / 18);
				g.setColor(Color.WHITE);
				g.drawRect(i * p.getWidth() / horizScale + horizontalOffset - 2, 
						height / vertScale * (vertScale - (j + 2)) - 17, 
						p.getWidth() / 16 - 5, height / 18);
				
				g.setColor(Color.WHITE);
				// customer ID
				text = queues.get(i)[j][0];
				x = i * p.getWidth() / horizScale + horizontalOffset;
				y = height / vertScale * (vertScale - (j + 2));
				g.drawString(text, x, y);
				
				// customer arrival time
				text = queues.get(i)[j][1];
				x = i * p.getWidth() / horizScale + horizontalOffset;
				y = height / vertScale * (vertScale - (j + 2)) + 14;
				g.drawString(text, x, y);
				
				// customer service time + wait time
				text = queues.get(i)[j][2];
				x = i * p.getWidth() / horizScale + horizontalOffset;
				y = height / vertScale * (vertScale - (j + 2)) + 28;
				g.drawString(text, x, y);
				
			}
			
			// clients served
			text = nonClientData.get(i)[0];
			x = i * p.getWidth() / horizScale + horizontalOffset;
			y = height / vertScale * (vertScale - 1);
			g.drawString(text, x, y);
			
			// avg wait
			text = nonClientData.get(i)[1];
			x = i * p.getWidth() / horizScale + horizontalOffset;
			y = height / vertScale * (vertScale - 1) + 14;
			g.drawString(text, x, y);

			
		}
		p.getGraphics().drawImage(bi, 0, 0, null);
	}
	
	public JButton getStartButton(){
		return startButton;
	}
	
	public String getTimeText(){
		return timeText.getText();
	}
	
	public String getDelayMSText(){
		return delayMSText.getText();
	}
	
	public String getMaxCustomersText(){
		return maxCustomersText.getText();
	}
	
	public String getNrQueuesText(){
		return nrQueuesText.getText();
	}
	
	public String getMaxQueuesText(){
		return maxQueuesText.getText();
	}
	
	public String getCustomerSpawnRateText(){
		return customerSpawnRateText.getText();
	}
	
	public String getMaxServiceTimeText(){
		return maxServiceTimeText.getText();
	}
	
}
