package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import model.Customer;
import model.Queue;
import view.GUI;

public class Controller {
	private GUI gui;
	private ArrayList<Queue> queues;
	private int clock = -1;
	private int time = 100; // need textbox
	private int delayMS = 100; // need textbox
	private int maxCustomers = 15; // need textbox
	private int nrQueues = 3; // need textbox
	private int maxQueues = 15; // need textbox
	private int customerSpawnRate = 30; // need textbox
	private int maxServiceTime = 100; // need textbox
	
	private PrintWriter out;
	private ActionListener al;
	
	private ArrayList<String[][]> queueStrings;
	private ArrayList<String[]> nonClientData; // clients served, avg wait time
	
	private Customer c;
	
	
	private float getAverageCustomers(){
		float sum = 0;
		for(int i = 0; i < nrQueues; i++)
			sum += queues.get(i).nrCustomers();
		return sum/nrQueues;
	}
	
	private void getData(){
		try {
			time = Integer.valueOf(gui.getTimeText());
			delayMS = Integer.valueOf(gui.getDelayMSText());
			maxCustomers = Integer.valueOf(gui.getMaxCustomersText());
			nrQueues = Integer.valueOf(gui.getNrQueuesText());
			maxQueues = Integer.valueOf(gui.getMaxQueuesText());
			customerSpawnRate = Integer.valueOf(gui.getCustomerSpawnRateText());
			maxServiceTime = Integer.valueOf(gui.getMaxServiceTimeText());
		} catch (NumberFormatException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}
	
	private void printInitialData(){
		// print
		out.println("Starting with " + nrQueues + " Queues");
		out.println("Maximum customers per Queue: " + maxCustomers);
		out.println("Maximum number of Queues allowed: " + maxQueues);
		out.println("Chance a customer will be created per clock cycle: " + customerSpawnRate + " percent");
		out.println("Maximum value of Service Time for customers: " + maxServiceTime);
		out.println("Clock will go for " + time + " cycles starting now...");
		out.println();
	}
	
	private void addQueues(){
		// adding queues
		for(int i = 0; i< nrQueues; i++){
			Queue temp = new Queue();
			temp.setDelay(delayMS);
			queues.add(temp);
			
			// print
			out.println("TIME: 0 - Created Queue " + String.valueOf(i));
		}
	}
	
	private void startQueues(){
		// starting the queue threads
		for(int i = 0; i< nrQueues; i++){
			queues.get(i).start();
			
			// print
			out.println("TIME: 0 - Started Queue " + String.valueOf(i));
		}
	}
	
	private void checkRemovedCustomers(){
		// checking to see which queues have removed which customers
		for(int i = 0; i<nrQueues; i++){
			int aux = queues.get(i).checkLastClient();
			if(aux != -1){
				// print
				out.println("TIME: " + clock + " - Queue " + i + " has served Customer: ID = " + aux);
			}
		}
	}
	
	private void printResults(){
		// print
		out.println();
		out.println("Results:");	
		out.println();
		out.println("Customers served:");
		// print
		for(int i = 0; i < nrQueues; i++){
			// print
			out.println("Queue " + i + ": " + queues.get(i).getCustomersServed());
		}
		out.println();
		
		// print
		out.println("Average wait time:");
		for(int i = 0; i < nrQueues; i++){
			// print
			out.println("Queue " + i + ": " + String.format( "%.2f", queues.get(i).getAverageWait()));
		}
		out.println();
		
		// print
		out.println("Empty time:");
		for(int i = 0; i < nrQueues; i++){
			// print
			out.println("Queue " + i + ": " + queues.get(i).getEmptyTime());
		}
		out.println();
	}
	
	private void moveCustomers(float avg){
		// we remove the last customer from each queue except the new one
		// until it will have less or equal customers than the average
		int j = 0;
		while(queues.get(nrQueues - 1).nrCustomers() < avg){
			if(queues.get(j).nrCustomers() > avg){
				// add it to the new queue
				Customer aux = queues.get(j).removeLastCustomer();
				queues.get(nrQueues - 1).enqueue( aux );
				//print
				out.println("TIME: " + clock + " - Moved Customer: ID = " + aux.getID() + " from Queue " + 
						j + " to Queue " + String.valueOf(nrQueues - 1));
			}
			j++;
			if(j == nrQueues)
				j = 0;
		}
	}
	
	private void draw(){
		queueStrings = new ArrayList<String[][]>();
		nonClientData = new ArrayList<String[]>();
		
		for(Queue i: queues){
			queueStrings.add(i.getCustomerStrings());
			String[] temp = new String[2];
			temp[0] = "C. Served: " + String.valueOf(i.getCustomersServed());
			temp[1] = "Avg Wait: " + String.format( "%.2f", i.getAverageWait());
			nonClientData.add(temp);
		}
		gui.updateInfo(queueStrings, nonClientData, clock);
		
		try {
			TimeUnit.MILLISECONDS.sleep(delayMS);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}
	
	private void addNewQueue(){
		if(nrQueues < maxQueues){
			// we add a new queue if allowed
			// and we add the new customer
			Queue temp = new Queue();
			temp.setDelay(delayMS);
			queues.add(temp);
			// print
			out.println("TIME: " + clock + " - Created Queue " + nrQueues);
			queues.get(nrQueues).enqueue(c);
			//print
			out.println("TIME: " + clock + " - Added new Customer: ID = " + c.getID() + " to Queue " + nrQueues 
					+ " Service Time = " + c.getST());
			queues.get(nrQueues).start();
			// print
			out.println("TIME: " + clock + " - Started Queue " + nrQueues);
			nrQueues++;
			// now we have to distribute the clients evenly
			float avg = getAverageCustomers(); // enough to compute it once, since it
			moveCustomers(avg);				   // won't change only by moving customers
		}else{
			// print
			out.println("TIME: " + clock + " - Tried to enqueue Customer: ID = " + c.getID() + " but all queues were full.");
		}
	}
	
	private void runClock(){
		Random r = new Random();
		while(clock < time){
			clock++;
			System.out.println(clock);
			checkRemovedCustomers();
			if(r.nextInt(100) + 1 <= customerSpawnRate){
				c = new Customer(clock, maxServiceTime);
				//getting the queue with the least customers
				int min = maxCustomers + 1;
				int minPointer = 0;
				for(int j = 0; j < nrQueues; j++)
					if(queues.get(j).nrCustomers() < min){
						min = queues.get(j).nrCustomers();
						minPointer = j;
					}
				if(min < maxCustomers){
					queues.get(minPointer).enqueue(c);
					// print
					out.println("TIME: " + clock + " - Added new Customer: ID = " + c.getID() + " to Queue " + minPointer 
							+ " Service Time = " + c.getST());
				}
				// check to see if queues are full
				else{ 
					addNewQueue();
				}
			}
			draw();
		} // end of while
	}
	
	private void initActionListener(){
		al = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clock = -1;
				try {
					out = new PrintWriter("output.txt");
				} catch (FileNotFoundException e2) {
					e2.printStackTrace();
				}
				getData(); // gets data from GUI
				queues = new ArrayList<Queue>();
				
				printInitialData(); // prints initial parameters to the output.txt file
				
				addQueues();
				startQueues();
				
				runClock(); // contains the while loop in which the simulation will run
				
				printResults(); // prints the results
				out.close();
			}
		};
	}
	
	public void init(){
		gui = new GUI();
		gui.setVisible(true);
		initActionListener();
		gui.getStartButton().addActionListener(al);
	}
	
}
