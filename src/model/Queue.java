package model;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Queue extends Thread{
	
	private ArrayList<Customer> customers;
	private int totalWaited;
	private int customersServed;
	private boolean enabled = true;
	private int delay_miliseconds = 1000;
	private final int MAX_SIZE = 16;
	private int lastCustomer = -1;
	private int emptyTime = 0;
	
	public Queue() {
		customers = new ArrayList<Customer>();
		customersServed = 0;
		totalWaited = 0;
		enabled = true;
		lastCustomer = -1;
		emptyTime = 0;
	}
	
	public int enqueue(Customer c) {
		if(customers.size() == MAX_SIZE){
			return -1;
		}
		else
			customers.add(c);
		return 0;
	}
	
	public void dequeue() {
		if(!customers.isEmpty()) {
			lastCustomer = customers.get(0).getID();
			customers.remove(0);
		}
	}
	
	// used if we want to move a customer in another queue
	public Customer removeLastCustomer(){
		if(customers.isEmpty())
			return null;
		Customer c = customers.get(customers.size() - 1);
		customers.remove(customers.size() - 1);
		c.resetWait();
		return c;
	}
	
	public void run() {
		while(enabled) {
			for(int i = 0; i < customers.size(); i++){
				customers.get(i).waitSec();
			}
			if(!customers.isEmpty()) {
				customers.get(0).decrementServiceTime();
		        if(currentWait() == 0) {
		        	customersServed++;
		        	totalWaited += customers.get(0).getWaited();
		        	dequeue();
		        }
			}else{
				emptyTime++;
			}
			
			try {
				TimeUnit.MILLISECONDS.sleep(delay_miliseconds);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
    }
	
	public String[][] getCustomerStrings() {
		String[][] ret = new String[customers.size()][3];
		for(int i = 0; i < customers.size(); i++)
			ret[i] = customers.get(i).toStrings();
		return ret;
	}
	
	public double getAverageWait() {
		if(customersServed != 0){
			return (double)totalWaited / customersServed;
		}
		else
			return totalWaited;
	}
	
	public int getCustomersServed(){
		return customersServed;
	}
	
	public int nrCustomers(){
		return customers.size();
	}
	
	public void closeQueue() {
		enabled = false;
	}
	
	public boolean isEmpty(){
		if(!customers.isEmpty())
			return true;
		return false;
	}
	
	private int currentWait() {
		if(customers.isEmpty())
			return 0;
		return customers.get(0).getST();
	}
	
	public void setDelay(int delay_miliseconds){
		this.delay_miliseconds = delay_miliseconds;
	}
	
	public int checkLastClient(){
		int aux = lastCustomer;
		lastCustomer = -1;
		return aux;
	}
	
	public int getEmptyTime(){
		return emptyTime;
	}
}
