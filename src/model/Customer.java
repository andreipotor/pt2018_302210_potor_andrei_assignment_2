package model;

import java.util.Random;

public class Customer {
	
	static int IDGenerator = 0;
	
	private int aT; // arrival time
	private int sT; // service time - time it takes to service the customer
	private int ID;
	private int waited; // seconds spent waiting by the customer
	
	Random r = new Random();
	
	public Customer(int time, int maxServiceTime) {
		aT = time;
		sT = r.nextInt(maxServiceTime) + 1;
		ID = IDGenerator++;
		waited = 0;
	}
	
	public int getAT() {
		return aT;
	}
	public void setAT(int arrivalTime) {
		this.aT = arrivalTime;
	}
	public int getST() {
		return sT;
	}
	public void setST(int serviceTime) {
		this.sT = serviceTime;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	} 
	
	public void decrementServiceTime() {
		sT--;
	}
	
	public String toString() {
		return "Customer ID:" + Integer.toString(ID) + "\nArrival time: " + Integer.toString(aT) + "\nService time: " + Integer.toString(sT);
	}
	
	public int getWaited(){
		return waited;
	}
	
	public void resetWait(){
		waited = 0;
	}
	
	public void waitSec(){
		waited++;
	}
	
	public String[] toStrings() {
		String[] res = new String[3];
		res[0] = "ID: " + Integer.toString(ID);
		res[1] = "Arrival time: " + Integer.toString(aT);
		res[2] = "Serve:" + Integer.toString(sT) + " Wait:" + Integer.toString(waited);
		return res;
	}
}
